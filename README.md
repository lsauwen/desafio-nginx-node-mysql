# Desafio Nginx Node MySql



## Baixando e executando o projeto

```
git clone git@gitlab.com:lsauwen/desafio-nginx-node-mysql.git

cd desafio-nginx-node-mysql/

docker-compose up -d
```

## Validando o projeto

```
http://localhost:8080
```